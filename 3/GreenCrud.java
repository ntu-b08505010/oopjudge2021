
/**
 * In GreenCrud, there is the method {@link #calPopulation}, and whose function is to calculate the size of the green crud population after a given amount of days, starting at a given initial size.
 * @author Yun-Chian
 */
public class GreenCrud {

	/**
	 * Returns an integer, which is the size of the green crud population after days days, starting at an initial size of initialSize.
	 * @param initialSize
	 * @param days
	 * @return final population
	 */
	public int calPopulation(int initialSize, int days) {
		return initialSize*fibonacci(days/5);
	}

	/**
	 * Calculate fibonacci numbers iteratively.
	 * @param n
	 * @return the nth fibonacci number
	 */
	private int fibonacci(int n) {
		int a = 0, b = 1, temp;
		for(int i = 0; i < n; ++i) {
			temp = b;
			b = a+b;
			a = temp;
		}
		return b;
	}
}
