/**
 * Contains info for an equilateral triangle.
 * @author Yun-Chian
 */
public class Triangle extends Shape {

	private static final double SQRT_3_OVER_4 = Math.sqrt(3)/4;

	/**
	 * Constructs the shape with the side length.
	 * @param length the side length of the triangle
	 */
	public Triangle(double length) {
		super(length);
	}

	/**
	 * {@inheritDoc} In this case, "length" is the side length.
	 * @param length the side length of the triangle
	 */
	@Override
	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public double getArea() {
		return Math.round(SQRT_3_OVER_4*length*length*100)/100D;
	}

	@Override
	public double getPerimeter() {
		return Math.round(3*length*100)/100D;
	}
}
