import java.util.function.DoubleFunction;

/**
 * 
 * @author Yun-Chian
 */
public class ShapeFactory {

	/**
	 * 
	 * @author Yun-Chian
	 *
	 */
	public enum Type {
		Triangle(Triangle::new), Square(Square::new), Circle(Circle::new),;

		private final DoubleFunction<Shape> constructor;

		private Type(DoubleFunction<Shape> constructor) {
			this.constructor = constructor;
		}
	}

	/**
	 * 
	 * @param shapeType
	 * @param length
	 * @return the shape created
	 */
	public Shape createShape(ShapeFactory.Type shapeType, double length) {
		return shapeType.constructor.apply(length);
	}
}
