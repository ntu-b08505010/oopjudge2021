/**
 * Contains info for a circle.
 * @author Yun-Chian
 */
public class Circle extends Shape {

	/**
	 * Constructs the shape with the diameter.
	 * @param length the diameter of the circle
	 */
	public Circle(double length) {
		super(length);
	}
	
	/**
	 * {@inheritDoc} In this case, "length" is the diameter.
	 * @param length the diameter of the circle
	 */
	@Override
	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public double getArea() {
		return Math.round(0.25*Math.PI*length*length*100)/100D;
	}

	@Override
	public double getPerimeter() {
		return Math.round(Math.PI*length*100)/100D;
	}
}
