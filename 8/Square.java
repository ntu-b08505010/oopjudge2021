/**
 * Contains info for a square.
 * @author Yun-Chian
 */
public class Square extends Shape {

	/**
	 * Constructs the shape with the side length.
	 * @param length the side length of the square
	 */
	public Square(double length) {
		super(length);
	}

	/**
	 * {@inheritDoc} In this case, "length" is the side length.
	 * @param length the side length of the square
	 */
	@Override
	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public double getArea() {
		return Math.round(length*length*100)/100D;
	}

	@Override
	public double getPerimeter() {
		return Math.round(4*length*100)/100D;
	}
}
