/**
 * Contains info for a simple geometric shape.
 * @author Yun-Chian
 */
public abstract class Shape {

	/**
	 * The "length" of the shape.
	 */
	protected double length;	

	/**
	 * Constructs the shape with the "length".
	 * @param length the "length"
	 */
	public Shape(double length) {
		this.length = length;
	}

	/**
	 * Set the "length" of this shape.
	 * @param length the "length"
	 */
	public abstract void setLength(double length);

	/**
	 * Gets the area of this shape, rounded to the second decimal place.
	 * @return the area
	 */
	public abstract double getArea();

	/**
	 * Gets the perimeter of this shape, rounded to the second decimal place.
	 * @return the perimeter
	 */
	public abstract double getPerimeter();

	/**
	 * Returns info with the area and perimeter of this shape.
	 * @return the info
	 */
	public String getInfo() {
		return "Area = "+getArea()+", Perimeter = "+getPerimeter();
	}
}