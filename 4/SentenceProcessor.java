import java.util.Arrays;

/**
 * In SentenceProcessor, there are two methods, which are {@link #removeDuplicatedWords} and {@link #replaceWord}, respectively.
 * @author Yun-Chian
 */
public class SentenceProcessor {

	/**
	 * Returns a string, which is sentence after removal of all duplicated words (leaving only the one that occurs first).
	 * <p>
	 * Implementation details:<br>
	 * The sentence is first separated into word using String.split. Then, a Stream is made from the resulting array and made distinct.
	 * It is then converted back into an array to be used in String.join for the result.
	 * This implementation is made to be as short as possible.
	 * @param sentence
	 * @return the processed sentence
	 */
	public String removeDuplicatedWords(String sentence) {
		return String.join(" ", Arrays.stream(sentence.split(" ")).distinct().toArray(String[]::new));
	}

	/**
	 * Returns a string, which is sentence after replacement of all occurrences of target with replacement.
	 * <p>
	 * Implementation details:<br>
	 * The sentence is first separated into word using String.split. Then, a Stream is made from the resulting array and the words are replaced.
	 * It is then converted back into an array to be used in String.join for the result.
	 * This implementation is made to be as short as possible.
	 * @param target
	 * @param replacement
	 * @param sentence
	 * @return the processed sentence
	 */
	public String replaceWord(String target, String replacement, String sentence) {
		return String.join(" ", Arrays.stream(sentence.split(" ")).map(word->word.equals(target)?replacement:word).toArray(String[]::new));
	}
}
