
/**
 * In APlusB, there is only a single static method, which is {@link #plus}, and whose function is to calculate the summation of two integers.
 * @author Yun-Chian
 * 
 */
public class APlusB {
	
	/**
	 * Returns an integer, which is the summation of a and b.
	 * @param a
	 * @param b
	 * @return a+b
	 */
	public static int plus(int a, int b) {
		return a+b;
	}
}
