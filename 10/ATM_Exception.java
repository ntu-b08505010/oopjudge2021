/**
 * Exception thrown by {@linkplain ATM_Service}.
 * @author Yun-Chian
 */
public class ATM_Exception extends Exception {

	private final ExceptionTYPE exceptionCondition;
	
	/**
	 * Constructs a new exception with the specified type.
	 * @param type
	 */
	public ATM_Exception(ExceptionTYPE type) {
		exceptionCondition = type;
	}

	@Override
	public String getMessage() {
		return exceptionCondition.name();
	}

	public enum ExceptionTYPE {
		BALANCE_NOT_ENOUGH, AMOUNT_INVALID,;
	}
}
