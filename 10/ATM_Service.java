/**
 * ATM service interface
 * @author Yun-Chian
 */
public interface ATM_Service {

	/**
	 * Checks if balance in user's account is sufficient.
	 * @param account the account
	 * @param money the amount of money
	 * @return true if balance in user's account is sufficient
	 * @throws ATM_Exception if balance in user's account is not sufficient
	 */
	public boolean checkBalance(Account account, int money) throws ATM_Exception;

	/**
	 * Checks if amount of money can be divided by 1000.
	 * @param money the amount of money
	 * @return true if amount of money can be divided by 1000
	 * @throws ATM_Exception if amount of money cannot be divided by 1000
	 */
	public boolean isValidAmount(int money) throws ATM_Exception;

	/**
	 * Tries to withdraw money from an account.
	 * @param account the account
	 * @param money the amount of money
	 */
	public void withdraw(Account account, int money);
}