/**
 * Contains info for an account.
 * @author Yun-Chian
 */
public class Account {

	private int balance;

	/**
	 * Constructs the account with the balance
	 * @param balance the balance
	 */
	public Account(int balance) {
		setBalance(balance);
	}

	/**
	 * @return the balance
	 */
	public int getBalance() {
		return balance;
	}

	/**
	 * @param the balance to set
	 */
	public void setBalance(int balance) {
		this.balance = balance;
	}
}