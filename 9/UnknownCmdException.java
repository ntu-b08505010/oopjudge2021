/**
 * Exception thrown by {@linkplain SimpleCalculator}.
 * @author Yun-Chian
 */
public class UnknownCmdException extends Exception {

	/**
	 * Constructs a new exception with the specified detail message.
	 * @param message
	 */
	public UnknownCmdException(String message) {
		super(message);
	}
}
