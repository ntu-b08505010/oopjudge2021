/**
 * Implements a simple calculator with 4 operators.
 * @author Yun-Chian
 */
public class SimpleCalculator {

	private double result = 0;
	private boolean firstCalc = true;
	private String msg;

	public SimpleCalculator() {
		msg = String.format("Calculator is on. Result = %.2f", result);
	}

	/**
	 * Parses the command and calculates the new result. <b>The end command is not supported in this method.</b>
	 * @param cmd the command
	 * @throws UnknownCmdException if the command is invalid
	 */
	public void calResult(String cmd) throws UnknownCmdException {
		String[] split = cmd.split(" ");
		if(split.length != 2) {
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		}
		String opStr = split[0];
		double val = validateInput(opStr, split[1]);
		switch(opStr) {
		case "+" -> result += val;
		case "-" -> result -= val;
		case "*" -> result *= val;
		case "/" -> {
			if(val == 0) {
				throw new UnknownCmdException("Can not divide by 0");
			}
			result /= val;
		}
		default -> throw new IllegalStateException("Unexpected error");
		}
		if(firstCalc) {
			msg = "Result %s %.2f = %.2f. New result = %.2f".formatted(opStr, val, result, result);
			firstCalc = false;
		}
		else {
			msg = "Result %s %.2f = %.2f. Updated result = %.2f".formatted(opStr, val, result, result);
		}
	}

	/**
	 * @return the message
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Checks if the command equals the end command, and sets the message accordingly.
	 * @param cmd the command
	 * @return if the calculation should end
	 */
	public boolean endCalc(String cmd) {
		if(cmd.equalsIgnoreCase("R")) {
			msg = "Final result = %.2f".formatted(result);
			return true;
		}
		return false;
	}

	/**
	 * Checks if an input to a calculation is valid.
	 * @param opStr the operator
	 * @param valStr the value
	 * @return the parsed value if both inputs are valid
	 * @throws UnknownCmdException if any input is invalid
	 */
	private double validateInput(String opStr, String valStr) throws UnknownCmdException {
		//merge two booleans and use switch
		byte flag = 0b11;
		//regex
		flag &= opStr.matches("[+*/-]") ? 0b11 : 0b10;
		double val = 0;
		try {
			val = Double.parseDouble(valStr);
		}
		catch(Exception e) {
			flag &= 0b01;
		}
		return switch(flag) {
		case 0b11 -> val;
		case 0b10 -> throw new UnknownCmdException(String.format("%s is an unknown operator", opStr));
		case 0b01 -> throw new UnknownCmdException(String.format("%s is an unknown value", valStr));
		case 0b00 -> throw new UnknownCmdException(String.format("%s is an unknown operator and %s is an unknown value", opStr, valStr));
		default -> throw new IllegalStateException("Unexpected error");
		};
	}
}
