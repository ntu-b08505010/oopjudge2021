/**
 * A simple implementation of an array-based list.
 * @author Yun-Chian
 */
public class SimpleArrayList {

	private Integer[] arr;

	/**
	 * Constructs the list with initial size 0.
	 */
	public SimpleArrayList() {
		arr = new Integer[0];
	}

	/**
	 * Constructs the list with initial size initialSize and sets all values to 0.
	 * @param initialSize the initial size
	 */
	public SimpleArrayList(int initialSize) {
		arr = new Integer[initialSize];
		for(int i = 0; i < initialSize; ++i) {
			arr[i] = 0;
		}
	}

	/**
	 * Adds an Integer to the end of the list.
	 * @param i the Integer to add
	 */
	public void add(Integer i) {
		Integer[] arrNew = new Integer[arr.length+1];
		System.arraycopy(arr, 0, arrNew, 0, arr.length);
		arr = arrNew;
		arr[arr.length-1] = i;
	}

	/**
	 * Returns the element at the specified position in this list. If the specified position is out of range of the list, returns null.
	 * @param index the index of the element
	 * @return the element at the specified position
	 */
	public Integer get(int index) {
		if(index >= arr.length) {
			return null;
		}
		return arr[index];
	}

	/**
	 * Replaces the element at the specified position in this list with the specified element, and returns the original element at that specified position. If the specified position is out of range of the list, returns null.
	 * @param index the index to replace
	 * @param element the element to replace with
	 * @return the original element at the specified position
	 */
	public Integer set(int index, Integer element) {
		if(index >= arr.length) {
			return null;
		}
		Integer original = arr[index];
		arr[index] = element;
		return original;
	}

	/**
	 * If a null element is at the specified position, returns false; otherwise removes it and returns true. Shifts any subsequent elements to the left if removes successfully.
	 * @param index the index to remove
	 * @return the original element at the specified position
	 */
	public boolean remove(int index) {
		if(index >= arr.length || arr[index] == null) {
			return false;
		}
		Integer[] arrNew = new Integer[arr.length-1];
		System.arraycopy(arr, 0, arrNew, 0, index);
		int numMoved = arr.length-index-1;
		if(numMoved > 0) {
			System.arraycopy(arr, index+1, arrNew, index, numMoved);
		}
		arr = arrNew;
		return true;
	}

	/**
	 * Removes all of the elements from this list.
	 */
	public void clear() {
		arr = new Integer[0];
	}

	/**
	 * Returns the number of elements in this list.
	 * @return the number of elements
	 */
	public int size() {
		return arr.length;
	}

	/**
	 * Retains only the elements in this list that are contained in the specified SimpleArrayList. In other words, removes from this list all of its elements that are not contained in the specified SimpleArrayList.
	 * @param l the specified list
	 * @return if the list changed
	 */
	public boolean retainAll(SimpleArrayList l) {
		int currIndex = 0;
		Integer[] arrNew = new Integer[arr.length];
		for(Integer i : arr) {
			if(l.contains(i)) {
				arrNew[currIndex++] = i;
			}
		}
		if(currIndex == arr.length) {
			return false;
		}
		Integer[] arrNew1 = new Integer[currIndex];
		System.arraycopy(arrNew, 0, arrNew1, 0, currIndex);
		arr = arrNew1;
		return true;
	}

	/**
	 * Returns if the element is in this list.
	 * @param i the element
	 * @return if the element is in this list
	 */
	public boolean contains(Integer i) {
		for(Integer j : arr) {
			if(i.equals(j)) {
				return true;
			}
		}
		return false;
	}
}
