
/**
 * In IsLeapYear, there is only a single method, which is {@link #determine}, and whose function is to determine whether or not a given year is a leap year.
 * @author Yun-Chian
 *
 */
public class IsLeapYear {

	/**
	 * Returns a boolean stating whether or not year is a leap year.
	 * @param year
	 * @return if the year is a leap year
	 */
	public boolean determine(int year) {
		return year % 400 == 0 || year % 100 != 0 && year % 4 == 0;
	}
}
