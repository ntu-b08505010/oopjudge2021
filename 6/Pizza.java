import java.util.Objects;

/**
 * Stores information about a single pizza.
 * @author Yun-Chian
 */
public class Pizza {

	private String size;
	private int numberOfCheese;
	private int numberOfPepperoni;
	private int numberOfHam;
	
	/**
	 * Default constructor that sets size to small and number of all toppings to 1.
	 */
	public Pizza() {
		this("small", 1, 1, 1);
	}
	
	/**
	 * Constructor that can set all of the instance variables.
	 * @param size
	 * @param numberOfCheese
	 * @param numberOfPepperoni
	 * @param numberOfHam
	 */
	public Pizza(String size, int numberOfCheese, int numberOfPepperoni, int numberOfHam) {
		this.size = size;
		this.numberOfCheese = numberOfCheese;
		this.numberOfPepperoni = numberOfPepperoni;
		this.numberOfHam = numberOfHam;
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * @return the numberOfCheese
	 */
	public int getNumberOfCheese() {
		return numberOfCheese;
	}

	/**
	 * @param numberOfCheese the numberOfCheese to set
	 */
	public void setNumberOfCheese(int numberOfCheese) {
		this.numberOfCheese = numberOfCheese;
	}

	/**
	 * @return the numberOfPepperoni
	 */
	public int getNumberOfPepperoni() {
		return numberOfPepperoni;
	}

	/**
	 * @param numberOfPepperoni the numberOfPepperoni to set
	 */
	public void setNumberOfPepperoni(int numberOfPepperoni) {
		this.numberOfPepperoni = numberOfPepperoni;
	}

	/**
	 * @return the numberOfHam
	 */
	public int getNumberOfHam() {
		return numberOfHam;
	}

	/**
	 * @param numberOfHam the numberOfHam to set
	 */
	public void setNumberOfHam(int numberOfHam) {
		this.numberOfHam = numberOfHam;
	}
	
	/**
	 * Returns the cost of the pizza, determined by:<br>
	 * Small: $10 + $2 per topping<br>
	 * Medium: $12 + $2 per topping<br>
	 * Large: $14 + $2 per topping
	 * @return the cost of the pizza.
	 */
	public double calcCost() {
		double basePrice;
		switch(size) {
		default:
		case "small":
			basePrice = 10; break;
		case "medium":
			basePrice = 12; break;
		case "large":
			basePrice = 14; break;
		}
		return basePrice+2*numberOfCheese+2*numberOfPepperoni+2*numberOfHam;
	}

	@Override
	public String toString() {
		return "size = "+size+", numOfCheese = "+numberOfCheese+", numOfPepperoni = "+numberOfPepperoni+", numOfHam = "+numberOfHam;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Pizza) {
			Pizza o = (Pizza)obj;
			return size.equals(o.getSize()) && numberOfCheese == o.numberOfCheese && numberOfPepperoni == o.numberOfPepperoni && numberOfHam == o.numberOfHam;
		}
		return false;
	}
}
