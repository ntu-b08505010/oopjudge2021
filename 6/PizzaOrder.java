
/**
 * Allows up to three pizzas to be saved in an order.
 * @author Yun-Chian
 */
public class PizzaOrder {

	private int numberPizzas = 1;
	private Pizza pizza1 = new Pizza();
	private Pizza pizza2 = new Pizza();
	private Pizza pizza3 = new Pizza();

	/**
	 * Sets the number of pizzas in the order. numberPizzas must be between 1 and 3. If numberPizzas is not in the range, the order is not valid and return false. Return true for valid order.
	 * @param numberPizzas the number of pizzas
	 * @return if the number was set
	 */
	public boolean setNumberPizzas(int numberPizzas) {
		if(numberPizzas >= 1 && numberPizzas <= 3) {
			this.numberPizzas = numberPizzas;
			return true;
		}
		return false;
	}

	/**
	 * @param pizza1 the pizza1 to set
	 */
	public void setPizza1(Pizza pizza1) {
		this.pizza1 = pizza1;
	}

	/**
	 * @param pizza2 the pizza2 to set
	 */
	public void setPizza2(Pizza pizza2) {
		this.pizza2 = pizza2;
	}

	/**
	 * @param pizza3 the pizza3 to set
	 */
	public void setPizza3(Pizza pizza3) {
		this.pizza3 = pizza3;
	}

	/**
	 * Calculates the total cost of the order
	 * @return the total cost
	 */
	public double calcTotal() {
		double total = pizza1.calcCost();
		if(numberPizzas >= 2) {
			total += pizza2.calcCost();
			if(numberPizzas == 3) {
				total += pizza3.calcCost();
			}
		}
		return total;
	}
}
